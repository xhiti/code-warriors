<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php $_SESSION['TrackingURL'] = $_SERVER["PHP_SELF"]; ?>

<?php
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM admins WHERE username='$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $role = $row["role"];

    if($role == "user"){
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeWarriors | About Us</title>
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
</head>
<body>
<!--NAVBAR-->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu">
                <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                <?php
                    if(checklogin() === true){ ?>
                        <li><a href="login.php" class="nav-links3">Logout</a></li>
                <?php } else {?>
                        <li><a href="login.php" class="nav-links2">Login</a></li>
                    <?php } ?>
            </ul>
            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
</nav>
<div style="height: 70px; background: #27aae1"></div>
<div class="blog-social">
    <h1>
        ABOUT US
        <br>
        <img src="https://image.ibb.co/nk616F/Layer_1_copy_21.png" width="47" height="11" align="center">
    </h1>
    <div class="social_icons">
        <div class="square">
            <div class="icons">
                <i class="fab fa-facebook-f fa-2x" aria-hidden="true"></i>
            </div>
        </div>
        <div class="square">
            <div class="icons">
                <i class="fab fa-instagram fa-2x" aria-hidden="true"></i>
            </div>
        </div>
        <div class="square">
            <div class="icons">
                <i class="fab fa-google-plus fa-2x" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="container" style="max-width: 1200px;">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-4 col-md-6 col-md-push-6">
            <div class="info-text right">
                <p>Welcome to Code Warriors, your number one source for the newest activity on music, sports, technology, social network and blog services.
                    We're dedicated to providing you the very best of sharing your ideas and get inspired by others creativity, with an emphasis on the newest trends but also the old classics. New ideas with a good experience makes a perfect combination.</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-6 col-md-pull-6">
            <div class="col-xs-12 col-sm-12">
                <figure class="snip1374"><a href="#"><img src="images/about_us_circle_2.jpg" alt="sample57" /></a></figure>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="container" style="max-width: 1200px;">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-6 col-md-pull-6">
            <div class="col-xs-12 col-sm-12">
                <figure class="snip1374"><a href="#"><img src="images/about_us_circle_1.jpg" alt="sample57" /></a></figure>
            </div>
        </div>
        <div class="col-xs-12 col-sm-8 col-sm-push-4 col-md-6 col-md-push-6">
            <div class="info-text right">
                <p>Founded in 2019 by a group of univesity friends, Code Warriors has come a long way from its beginnings.
                    When  first started out, their passion for sharing exoeriences and getting in touch with everyone aroud the world drove them to start their own business.
                    We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.</p>
            </div>
        </div>
    </div>
</div>
<br>
<div class='information-container'>
    <div class='inner-container'>
        <h1 class='section-title'>Informations</h1>
        <div class='border'></div>
        <div class='service-container'>
            <div class='service-box'>
                <div class='service-icon'>
                    <i class='fas fa-share'></i>
                </div>
                <div class='service-title'>Share your thoughts </div>
                <div class='description'>
                    Make a post in our blog to share it with your friends and find out even more or you just want to publish your achievement?
                </div>
            </div>
            <div class='service-box'>
                <div class='service-icon'>
                    <i class='fas fa-comment-dots'></i>
                </div>
                <div class='service-title'>Express your opinion</div>
                <div class='description'>
                    You know something more about a post, you can always add a comment to one of your posts or your friends posts.
                </div>
            </div>

            <div class='service-box'>
                <div class='service-icon'>
                    <i class='fas fa-thumbs-up'></i>
                </div>
                <div class='service-title'>Express your approval</div>
                <div class='description'>
                    You can always tell whether  you  agree, like, disapprove or hate a post just by clicking a button.
                </div>
            </div>

            <div class='service-box'>
                <div class='service-icon'>
                    <i class='fas fa-user-edit'></i>
                </div>
                <div class='service-title'>Edit Sections</div>
                <div class='description'>
                    You can edit your profile or your published posts anytime.
                </div>
            </div>

            <div class='service-box'>
                <div class='service-icon'>
                    <i class='fas fa-check-double'></i>
                </div>
                <div class='service-title'>Actions</div>
                <div class='description'>
                    You can also reply to one the comment if something relates to it.
                </div>
            </div>

            <div class='service-box'>
                <div class='service-icon'>
                    <i class='fas fa-question'></i>
                </div>
                <div class='service-title'>Anymore questions ?</div>
                <div class='description'>
                    Feel  free to ask us in the Contacts page. We are available 24/7.
                </div>
            </div>

        </div>
    </div>
</div>
<?php require_once ('includes/footer.php'); ?>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</html>
<?php }
        else{ header("location: dashboard.php");?>
            <div class="container">
                <?php $_SESSION["ErrorMessage"] = "You are not allowed to do this operation"; ?>
            </div>
        <?php  }?>