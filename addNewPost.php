<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php $_SESSION['TrackingURL'] = $_SERVER["PHP_SELF"]; ?>
<?php
    global $user_id;
    confirmLogin();
?>
<?php
    $username = $_SESSION['username'];

if (isset($_POST["publish_post_button"])){
    $username = $_SESSION['username'];
    $sql = "select * from admins where username = '$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $user_id = $row[0];

    $category_id = $_POST['categoryID'];
    $title = $_POST["postTitle"];
    $image = $_FILES["image"]["name"];
    $target = "upload/".basename($_FILES["image"]["name"]);
    $post = $_POST["postDescription"];

    date_default_timezone_set("Europe/London");
    $currentTime = time();
    $dateTime = strftime("%B-%d-%Y %H:%M:%S", $currentTime);

    if (!isset($image)){
        $_SESSION["ErrorMessage"] = "Image can't be empty!";
    }

    if (empty($title)){
        $_SESSION["ErrorMessage"] = "Title can't be empty!";
    }
    elseif (strlen($title) < 5){
        $_SESSION["ErrorMessage"] = "Post title should be greater than 5 characters!";
    }
    elseif (strlen($post) > 1999){
        $_SESSION["ErrorMessage"] = "Post description should be less than 2000 characters!";
    }
    else{
        $query = "insert into posts(dateTime, title, image, post, category_id, user_id) values (?, ?, ?, ?, ?, ?)";
        $result = mysqli_prepare($connectingDB, $query);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target);

        if ($result){
            mysqli_stmt_bind_param($result, "ssssss", $dateTime, $title, $image, $post, $category_id, $user_id);
            mysqli_stmt_execute($result);
            $last_id = mysqli_insert_id($connectingDB);
            $_SESSION["SuccessMessage"] = "Post with id: ".$last_id." added successfully";
            //redirect_to("addNewPost.php");
        }
        else{
            $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
        }
    }
}
?>
<?php
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM admins WHERE username='$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $role = $row["role"];

    if($role == "user"){
        ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeWarriors | Posts</title>
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/contactus.css">
    <link rel="stylesheet" href="css/footer.css">
</head>
<body>
<!--NAVBAR-->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu">
                <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                <li><a href="login.php" class="nav-links3">Logout</a></li>
            </ul>
            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
</nav>
<div style="height: 70px; background: #27aae1">
</div>
<!-- Latest POSTS Area Start -->
<div class="col-md-12 add_post_head" style="height: 130px; width: 100%; padding: 0!important;">
    <div class="contact100-more flex-col-c-m" style="background-image: url('images/contact_us_bg.jpg'); width: 100%; padding: 130px 15px 0px 15px!important;">
        <div class="sp-container">
            <div class="sp-content">
                <div class="sp-globe"></div>
                <h2 class="frame-1">Have any idea?</h2>
                <h2 class="frame-2">Create your post</h2>
                <h2 class="frame-3">Share your thoughts</h2>
                <h2 class="frame-4">Share your everything you want to!</h2>
                <h2 class="frame-5">
                    <span>Ideas,</span>
                    <span>Problems,</span>
                    <span>Solutions.</span>
                </h2>
            </div>
        </div>
    </div>
</div>
<div class="container3" style="background-image: url('images/contact_us_bg.jpg')">
    <div style="padding: 30px; width: 100%;">
        <div class="col-sm-8" style="border-radius: 5px; background-color: white; float: right;">
            <form class="contact100-form validate-form" action="addNewPost.php" method="post" enctype="multipart/form-data">
                <div class="mb-3">
                    <h2 style="padding: 30px;">Add New Post</h2><br>
                    <?php echo errorMessage(); echo successMessage(); ?>
                    <div>
                        <div class="wrap-input100 validate-input">
                            <label for="title"><span class="label-input100">Post title: </span></label>
                            <input type="text" class="input100" name="postTitle" id="title" placeholder="Type title here...">
                            <span class="focus-input100"></span>
                        </div><br>
                        <div class="wrap-input100 validate-input">
                            <label for="categoryTitle"><span class="label-input100">Choose category: </span></label>
                            <select class="input100" id="categoryTitle" name="categoryID">
                                <?php
                                    global $connectingDB;
                                    $sql = "select id, title from category order by title asc";
                                    $result = mysqli_query($connectingDB, $sql);

                                    while ($row = mysqli_fetch_array($result)) {
                                        $category_id = $row[0];
                                        $title = $row['title'];
                                        echo '<option value="'.$category_id.'" >'.$title.'</option>';
                                    }
                                ?>
                            </select>
                            <span class="focus-input100"></span>
                        </div><br>
                        <div class="wrap-input100 validate-input mb-1">
                            <label for="imageSelect"><span class="label-input100">Select Image</span></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input input100" name="image" id="imageSelect">
                                <label for="imageSelect" class="custom-file-label">Select Image</label>
                                <span class="focus-input100"></span>
                            </div>
                        </div><br>
                        <div class="wrap-input100 validate-input">
                            <label for="post" ><span class="label-input100">Post:</span></label>
                            <textarea class="input100" name="postDescription" id="post" rows="8" cols="80"></textarea>
                            <span class="focus-input100"></span>
                        </div><br>
                        <div class="row">
                            <div class="col-lg-6 mb-2">
                                <a href="blog.php" class="btn btn-warning btn-block" style="background-color: #27aae1; color: white"><i class="fas fa-arrow-left"></i> Back to Blog</a>
                            </div>
                            <div class="col-lg-6">
                                <button type="submit" name="publish_post_button" style="background-color: #0C0613" class="btn btn-success btn-block">
                                    <i class="fas fa-check"></i> Publish
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Latest POST Area End -->
<?php require_once ('includes/footer.php'); ?>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
<script data-cfasync="false" type="text/javascript" src="js/form-submission-handler.js"></script>
</html>
<?php }
        else{ header("location: dashboard.php");?>
            <div class="container">
                <?php $_SESSION["ErrorMessage"] = "You are not allowed to do this operation"; ?>
            </div>
        <?php  }?>
