<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php
    global $user_id;
    /*confirmLogin();*/
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <title>CodeWarriors | Blog Page</title>
    <style media="screen">
        .heading{
            font-family: Bitter,Georgia,"Times New Roman",Times,serif;
            font-weight: bold;
            color: #005E90;
        }
        .heading:hover{
            color: #0090DB;
        }

        .statistika{
            color:black;
            font-size:20px;
        }
    </style>
</head>
<body>
<!-- NAVBAR -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php?page=1"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <?php
            if (checklogin() === true){
            global $role;
            $username = $_SESSION['username'];
            $sql      = "select * from admins where username = '$username'";
            $result   = mysqli_query($connectingDB, $sql);
            $row      = mysqli_fetch_array($result);
            $role     = $row['role'];

            if ($role === 'user'){
            ?>
                <ul class="sm-nav-menu">
                    <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                    <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                    <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                    <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                    <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                    <?php
                    if (checklogin() === true){?>
                        <li><a href="login.php" class="nav-links3">Logout</a></li>
                    <?php } else {?>
                        <li><a href="login.php" class="nav-links2">Login</a></li>
                    <?php } ?>
                </ul>
                <ul style="float:right;" class="navbar-nav ml-auto">
                    <form class="form-inline d-none d-sm-block" action="blog.php">
                        <div class="form-group">
                            <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                            <button  class="btn btn-primary" name="SearchButton">Go</button>
                        </div>
                    </form>
                </ul>
            <?php }
            elseif ($role === 'admin') { ?>
                <nav class="sm-navbar navbar navbar-expand-lg">
                    <div class="container2">
                        <div class="sm-logo">
                            <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
                            <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                                <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                                <li><a href="posts.php" class="nav-links2">Posts</a></li>
                                <li><a href="categories.php" class="nav-links2">Categories</a></li>
                                <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                                <li><a href="comments.php" class="nav-links2">Comments</a></li>
                                <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                                <li><a href="login.php" class="nav-links3">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            <?php } }
            elseif (checklogin() != true){ ?>
                <ul class="sm-nav-menu">
                    <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                    <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                    <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                    <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                    <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                    <li><a href="login.php" class="nav-links2">Login</a></li>
                </ul>
           <?php } ?>
        </div>
    </div>
</nav>
<div style="height:10px; background:#27aae1;"></div>
<!-- NAVBAR END -->
<!-- HEADER -->
<div class="container">
    <div class="row mt-4">
        <!-- Main Area Start-->
        <div class="col-sm-8 "> <br><br><br>
            <h2 style="color: #0C0613;  ">CodeWarriors <span style="font-weight: bold">Blog</span></h2><br><br>
            <!-- <h1 style="color: #0C0613; font-weight: bold">CODE WARRIORS Blog</h1> <br><br> -->
            <?php
                echo ErrorMessage();
                echo SuccessMessage();
            ?>
            <?php
            global $connectingDB;
            // SQL query when Searh button is active

            if(isset($_GET["SearchButton"])){
                $Search = $_GET["Search"];
                $sql = "SELECT * FROM posts
          WHERE datetime LIKE '%{$Search}%'
          OR title LIKE '%{$Search}%'
          OR post LIKE '%{$Search}%'";
                $result = mysqli_query($connectingDB, $sql);
            }// Query When Pagination is Active i.e Blog.php?page=1
            elseif (isset($_GET["page"])) {
                $Page = $_GET["page"];
                if($Page==0||$Page<1){
                    $ShowPostFrom=0;
                }else{
                    $ShowPostFrom=($Page*5)-5;
                }
                $sql ="SELECT * FROM posts ORDER BY id desc LIMIT $ShowPostFrom,5";
                $result = mysqli_query($connectingDB, $sql);
            }
            // Query When Category is active in URL Tab
            elseif (isset($_GET["category"])) {
                $Category = $_GET["category"];
                // $sql = "SELECT * FROM posts WHERE category_id=$Category ORDER BY id desc";
                $sql = "SELECT * FROM posts p JOIN category c ON p.category_id = c.id
                  WHERE category_id=$Category order by p.id desc";
                $result = mysqli_query($connectingDB, $sql);
            }
            // The default SQL query
            else{
                $sql  = "SELECT * FROM posts ORDER BY id desc";
                $result = mysqli_query($connectingDB, $sql);
            }

            while ($DataRows = mysqli_fetch_array($result)) {
                $PostId          = $DataRows[0];
                $DateTime        = $DataRows["dateTime"];
                $PostTitle       = $DataRows[2];
                $CategoryId      = $DataRows["category_id"];
                $Image           = $DataRows["image"];
                $PostDescription = $DataRows["post"];
                $user_id         = $DataRows["user_id"];

                ?>
                <img src="upload/<?php echo htmlentities($Image); ?>" class="img-fluid card-img-top" style="height: 500px!important;">
                <div class="card"> <br>
                    <h4 class="card-title" style="color: #0C0613; font-weight: bold; padding-left: 20px"><?php echo htmlentities($PostTitle); ?></h4>
                    <div class="card-body">
                        <small class="text-muted">Category: <span class="text-dark">
               <a style="color:darkblue;" href="blog.php?category=<?php echo $CategoryId; ?>"> <?php
                   global $connectingDB;
                   $sql = "SELECT * FROM category WHERE id=$CategoryId ";
                   $res = mysqli_query($connectingDB, $sql);
                   $row = mysqli_fetch_array($res);
                   echo htmlentities($row["title"]); ?> </a> &nbsp;
              </span> & Written by <span style="color:darkblue;" class="text-dark"> <a style="color:darkblue;"><b>
                <?php
                global $connectingDB;
                $sql = "SELECT * FROM admins WHERE id=$user_id";
                $res = mysqli_query($connectingDB, $sql);
                $Row = mysqli_fetch_array($res);
                echo htmlentities($Row["aname"]); ?>
              </b></a></span> &nbsp; On <span style="color:darkblue;" class="text-dark"><b><?php echo htmlentities($DateTime); ?></b></span></small>
                        <span style="float: right;" class="badge" >
                <i class="fa fa-thumbs-up statistika"></i> <span class="statistika" id="nrlikes"> <?php echo getLikes($PostId); ?> </span> &nbsp;
                <i class="fa fa-thumbs-down statistika"></i> <span class="statistika" id="nrdislikes"> <?php echo getDislikes($PostId); ?> </span> &nbsp;
                <i class="fas fa-comment-alt statistika"></i> <span class="statistika" id="nrcomments"> <?php echo ApproveCommentsAccordingtoPost($PostId); ?> </span>
            </span>
                        <hr>
                        <p class="card-text">
                            <?php if (strlen($PostDescription)>500) { $PostDescription = substr($PostDescription,0,500)."...";} echo htmlentities($PostDescription); ?></p>
                        <a href="FullPost.php?id=<?php echo $PostId; ?>" style="float:right;">
                            <span class="btn btn-info">Read More &rang;&rang; </span>
                        </a>
                    </div>
                </div>
                <br>
                <br>
            <?php   } ?>
            <!-- Pagination -->
            <nav>
                <ul class="pagination pagination-lg">
                    <!-- Creating Backward Button -->
                    <?php if( isset($Page) ) {
                        if ( $Page>1 ) {?>
                            <li class="page-item">
                                <a href="blog.php?page=<?php  echo $Page-1; ?>" class="page-link">&laquo;</a>
                            </li>
                        <?php } }?>
                    <?php
                    global $connectingDB;
                    $sql           = "SELECT COUNT(*) FROM posts";
                    $result         = mysqli_query($connectingDB, $sql);
                    $RowPagination = mysqli_fetch_array($result);
                    $TotalPosts    = array_shift($RowPagination);
                    // echo $TotalPosts."<br>";
                    $PostPagination=$TotalPosts/5;
                    $PostPagination=ceil($PostPagination);
                    // echo $PostPagination;
                    for ($i=1; $i <=$PostPagination ; $i++) {
                        if( isset($Page) ){
                            if ($i == $Page) {  ?>
                                <li class="page-item active">
                                    <a href="blog.php?page=<?php  echo $i; ?>" class="page-link"><?php  echo $i; ?></a>
                                </li>
                                <?php
                            }else {
                                ?>  <li class="page-item">
                                    <a href="blog.php?page=<?php  echo $i; ?>" class="page-link"><?php  echo $i; ?></a>
                                </li>
                            <?php  }
                        } } ?>
                    <!-- Creating Forward Button -->
                    <?php if ( isset($Page) && !empty($Page) ) {
                        if ($Page+1 <= $PostPagination) {?>
                            <li class="page-item">
                                <a href="blog.php?page=<?php  echo $Page+1; ?>" class="page-link">&raquo;</a>
                            </li>
                        <?php } }?>
                </ul>
            </nav>
        </div>
        <!-- Main Area End-->

        <!-- Side Area Start -->
        <div class="col-sm-4" style="padding-top: 137px;">
            <div class="card mt-4">
                <img src="./images/startblog.PNG" alt="">
                <div class="card-body">
                    <p>Start a blog now & create your posts!</p>
                    <p>Make a post in our blog to share it with your friends and find out even more or you just want to publish your achievement?</p>
                    <p>Share your ideas and much more!</p>
                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-header text-light">
                    <h3 style="font-weight: bold">CodeWarriors</h3>
                </div>
                <div class="card-body">
                    <a href="statistics.php" class="btn btn-success btn-block text-center text-white mb-4" name="button">Go to statistics</a>
                    <a href="addNewPost.php" class="btn btn-danger btn-block text-center text-white mb-4" name="button">Create your own post </a>
                    <div class="input-group mb-3" style="width: 100%;">
                        <div class="input-group-append" style="width: 100%">
                            <a href="myprofile.php" style="width: 100%" class="btn btn-primary block text-center text-white mb-4" name="button">Look at your profile </a>
                        </div>
                    </div>
                </div>
            </div>
            <br>

            <!-- START CATEGORIES -->
            <div class="card">
                <div class="pl20 pb30 clear" style="padding: 30px;">
                    <h3 style="font-weight: bold">Categories</h3><br>
                    <div class="categories widget clear">
                        <?php
                            global $connectingDB;
                            $sql = "SELECT * FROM category ORDER BY title asc";
                            $result = mysqli_query($connectingDB, $sql);
                            while ($DataRows = mysqli_fetch_array($result)){
                                $CategoryId = $DataRows["id"];
                                $CategoryName=$DataRows["title"];
                                ?>
                                <div class="category rel" style="height: 25px">
                                    <a href="blog.php?category=<?php echo $CategoryId; ?>"><p class="cn name"><?php echo $CategoryName; ?></p></a><br>
                                </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <!-- END CATEGORIES -->
            <br>
            <div class="card">
                <div class="pl20 pb30 clear" style="padding: 30px;">
                    <h3 style="font-weight: bold">Latest Posts</h3>
                    <div class="trending-widget" style="padding-top: 30px;">
                        <div class="trending-widget" style="padding-top: 30px;">
                            <?php
                            global $connectingDB;
                            $sql= "SELECT * FROM posts ORDER BY id desc LIMIT 0,5";
                            $result = mysqli_query($connectingDB, $sql);
                            while ($DataRows = mysqli_fetch_array($result)) {
                                $Id     = $DataRows['id'];
                                $Title  = $DataRows['title'];
                                $DateTime = $DataRows['dateTime'];
                                $Image = $DataRows['image'];
                                ?>
                                <div class="tw-item" style="border-left: 5px solid #27aae1; margin-bottom: 10px">
                                    <div class="tw-thumb">
                                        <img src="upload/<?php echo htmlentities($Image); ?>" class="trending_posts" style="width: 115px; height: 78px;" alt="#">
                                    </div>
                                    <div class="tw-text">
                                        <div class="tw-meta" style="font-size: 8px"><?php echo htmlentities($DateTime); ?></div>
                                        <a style="text-decoration:none;" href="fullPost.php?id=<?php echo htmlentities($Id) ; ?>" target="_blank"><h5 style="font-weight: bold; color: #0C0613"><?php if (strlen($Title) > 20) { $Title = substr($Title, 0, 20)."..."; } echo htmlentities($Title); ?></h5></a>
                                    </div>
                                </div>
                                <br>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Side Area End -->
    </div>
</div>
<!-- HEADER END -->
<br>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script>
    $('#year').text(new Date().getFullYear());
</script>
<?php require("includes/footer.php"); ?>
</body>
</html>
