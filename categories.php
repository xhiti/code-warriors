<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php confirmLogin(); ?>
<?php
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM admins WHERE username='$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $role = $row["role"];

    if ($role == "admin") {

    ?>
<?php
if (isset($_POST["publish_button"])){
    $category = $_POST["categoryTitle"];
    $admin = "CodeWarriors";

    date_default_timezone_set("Europe/Tirane");
    $currentTime = time();
    $dateTime = strftime("%B-%d-%Y %H:%M:%S", $currentTime);

    if (empty($category)){
        $_SESSION["ErrorMessage"] = "All fields must be filled out!";
        header("Location: categories.php");
    }
    elseif (strlen($category) < 3){
        $_SESSION["ErrorMessage"] = "Category title should be greater than 3 characters!";
        header("Location: categories.php");
    }
    elseif (strlen($category) > 50){
        $_SESSION["ErrorMessage"] = "Category title should be less than 50 characters!";
        header("Location: categories.php");
    }
    else{
        $query = "insert into category(title, author, dateTime) values (?, ?, ?)";
        $result = mysqli_prepare($connectingDB, $query);

        if ($result){
            mysqli_stmt_bind_param($result, "sss", $category, $admin, $dateTime);
            mysqli_stmt_execute($result);
            $last_id = mysqli_insert_id($connectingDB);
            $_SESSION["SuccessMessage"] = "Category with id: ".$last_id." added successfully";
            header("Location: categories.php");

        }
        else{
            $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
            header("Location: categories.php");
        }
        mysqli_stmt_close($result);
        mysqli_close($connectingDB);
    }
}
?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>CodeWarriors | Categories</title>
        <meta charset="UTF-8">
        <!-- Favicon -->
        <link href="images/first.jpg" rel="shortcut icon"/>
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
        <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
        <!-- Stylesheets -->
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/owl.carousel.min.css"/>
        <link rel="stylesheet" href="css/about.css"/>
        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="css/footer.css">
        <!-- Main Stylesheets -->
        <link rel="stylesheet" href="css/index.css"/>
    </head>
    <body>

    <!-- Header section -->
    <nav class="sm-navbar navbar navbar-expand-lg">
        <div class="container2">
            <div class="sm-logo">
                <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbarcollapseCMS">
                <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                    <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                    <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                    <li><a href="posts.php" class="nav-links2">Posts</a></li>
                    <li><a href="categories.php" class="nav-links2">Categories</a></li>
                    <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                    <li><a href="comments.php" class="nav-links2">Comments</a></li>
                    <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                    <li><a href="login.php" class="nav-links3">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div style="height:50px; background:#27aae1;"></div>
    <header class="bg-dark text-white py-3">
        <hr class="colorgraph" style="width: 100%!important;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 style="margin-top:-100px;" ><i class="fas fa-list-alt" style="color: #27aae1;"></i> Manage Categories</h1>
                </div>
            </div>
        </div>
    </header>
    <!--HEADER END-->
    <br>

    <!--MAIN AREA-->
    <section class="container2 py-2 mb-4">
        <div class="row mt-4" style="width: 100%;">
            <div class="offset-lg-1 col-lg-10">
                <?php echo errorMessage(); echo successMessage(); ?>
                <form class="" action="categories.php" method="post">
                    <div class="card bg-secondary text-light mb-3">
                        <div class="card-header" style="background-color: white; border: #0C0613 5px">
                            <h3>Add New <span style="font-weight: bold">Category</span></h3>
                        </div>
                        <div class="card-body bg-dark">
                            <div class="form-group">
                                <label for="title"><span class="fieldInfo">Category title: </span></label>
                                <input type="text" class="form-control" name="categoryTitle" id="title" placeholder="Type title here...">
                            </div>
                            <div class="row">
                                <div class="col-lg-6 mb-2">
                                    <a href="dashboard.php" class="btn btn-warning btn-block"><i class="fas fa-arrow-left"></i> Back to Dashboard</a>
                                </div>
                                <div class="col-lg-6">
                                    <button type="submit" name="publish_button" class="btn btn-success btn-block">
                                        <i class="fas fa-check"></i> Publish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <br><br>
                <h2>Existing <span style="font-weight: bold">Categories</span></h2><br>
                <table class="table table-striped table-hover">
                    <thead class="table-dark" style="background-color: #0C0613">
                    <tr>
                        <th>#</th>
                        <th>Date&Time</th>
                        <th>Category Name</th>
                        <th>Creator Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <?php
                    global $connectingDB;
                    $sql = "SELECT * FROM category ORDER BY title asc";
                    $result = mysqli_query($connectingDB, $sql);
                    $SrNo = 0;
                    while ($DataRows = mysqli_fetch_array($result)) {
                        $CategoryId = $DataRows["id"];
                        $CategoryDate = $DataRows["dateTime"];
                        $CategoryName = $DataRows["title"];
                        $CreatorName= $DataRows["author"];
                        $SrNo++;
                        ?>
                        <tbody>
                        <tr>
                            <td><?php echo htmlentities($SrNo); ?></td>
                            <td><?php echo htmlentities($CategoryDate); ?></td>
                            <td><?php echo htmlentities($CategoryName); ?></td>
                            <td><?php echo htmlentities($CreatorName); ?></td>
                            <td> <a href="deleteCategory.php?id=<?php echo $CategoryId;?>" class="btn btn-danger">Delete</a>  </td>
                        </tr>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </section>
    <!--END MAIN AREA-->

    <?php require("includes/footer.php"); ?>
    </body>
    </html>
<?php } else {
    $_SESSION["ErrorMessage"] = "You are not allowed to do this operation";
    header("location: blog.php?page=1");
} ?>