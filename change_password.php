<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php
    global $connectingDB;
    global $password;

    $username = $_SESSION['username'];
    $sql = "select * from admins where username = '$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);

    if (isset($_POST["change_pass"])) {

        $new_password = strip_tags($_POST["new_password"]);
        $new_confirm_password = strip_tags($_POST["new_confirm_password"]);

        date_default_timezone_set("Europe/London");
        $currentTime = time();
        $dateTime = strftime("%B-%d-%Y %H:%M:%S", $currentTime);

        if (empty($new_password) || empty($new_confirm_password)){
            $_SESSION["ErrorMessage"] = "All fields must be filled out!";
        }
        elseif (strlen($new_password) < 5){
            $_SESSION["ErrorMessage"] = "New password should be greater than 5 characters!";
        }
        elseif ($new_password !== $new_confirm_password){
            $_SESSION["ErrorMessage"] = "New password and Confirm password should match!";
        }
        elseif (preg_match('/[^A-Za-z0-9]/', $new_password)){
            $_SESSION["ErrorMessage"] = "Your password can contain only english characters and numbers!";
        }
        else{
            $new_password = md5($new_password);

            $sql = "UPDATE admins SET password  = '$new_password' WHERE username= '$username'";
            $result = mysqli_query($connectingDB, $sql);

            if ($result){
                $_SESSION["SuccessMessage"] = "Password changed successfully!";
                header("location: login.php");
            }
            else{
                $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
            }
            mysqli_stmt_close($result);
            mysqli_close($connectingDB);
        }
    }
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>CodeWarriors | Change Password</title>
    <meta charset="UTF-8">
    <!-- Favicon -->
    <link href="images/first.jpg" rel="shortcut icon"/>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.min.css"/>
    <style>
        /*change password*/
        html, body {
            margin: 0;
            padding: 0;
            font-family: 'Raleway', sans-serif;
            min-height: 100%;
        }
        body {
            background: #f6f6f6 url(images/contact_us_bg.jpg) top center no-repeat;
            background-size:cover;
            font-size: 18px;
            margin:0
        }
        #box {
            width: 400px;
            margin: 10% auto;
            text-align: center;
            background:rgba(255,255,255,1);
            padding:20px 50px;
            box-sizing:border-box;
            box-shadow:0 3px 12px rgba(0,0,0,0.9);
            border-radius:2%
        }
        #box h1 {
            margin-bottom: 1.5em;
            font-size: 30px;
            color: #484548;
            font-weight: 100;
        }
        #box h1 span, small {
            display:block;
            font-size: 14px;
            color: #989598;
        }
        #box small{ font-style: italic; font-size: 11px;}
        #box form p { position: relative; }

        #box .password {
            width: 90%;
            padding: 15px 12px;
            margin-bottom: 5px;
            border: 1px solid #e5e5e5;
            border-bottom: 2px solid #ddd;
            background: rgba(255,255,255,0.2) !important;
            color: #555;
        }
        #box .password + .unmask {
            position: absolute;
            right: 8%;
            top: 15px;
            width: 25px;
            height: 25px;
            background: transparent;
            border-radius: 50%;
            cursor: pointer;
            border: none;
            font-family: 'fontawesome';
            font-size: 14px;
            line-height: 24px;
            -webkit-appearance: none;
            outline: none;
        }
        #box .password + .unmask:before {
            content: "\f06e";
            position:absolute;
            top:0; left:0;
            width: 25px;
            height: 25px;
            background: rgba(205,205,205,0.2);
            z-index:1;
            color:#aaa;
            border:2px solid;
            border-radius: 50%;
        }
        #box .password[type="text"] + .unmask:before {
            content:"\f070";
            background: rgba(105,205,255,0.2);
            color:#06a
        }
        #valid{
            font-size:12px;
            color:#daa;
            height:15px
        }
        #strong{
            height:20px;
            font-size:12px;
            color:#daa;
            text-transform:capitalize;
            background:rgba(205,205,205,0.1);
            border-radius:5px;
            overflow:hidden
        }

        #strong span{
            display:block;
            box-shadow:0 0 0 #fff inset;
            height:100%;
            transition:all 0.8s
        }
        #strong .weak{
            box-shadow:5em 0 0 #daa inset;
        }
        #strong .medium{
            color:#da6;
            box-shadow:10em 0 0 #da6 inset
        }
        #strong .strong{
            color:#595;
            box-shadow:50em 0 0 #ada inset
        }
    </style>
</head>
<body>
    <div id="box">
        <?php echo errorMessage(); echo successMessage(); ?>
        <form id="myform-search" class="validate-form" action="change_password.php" method="post" autocomplete="off" >
            <h1 style="font-weight: bold">Change Password <span style="font-weight: bold">choose a good one!</span></h1>
            <p>
                <input type="password" value="" name="new_password" placeholder="Enter Password" id="p" class="password">
                <button class="unmask" type="button"></button>
            </p>
            <p>
                <input type="password" value="" name="new_confirm_password" placeholder="Confirm Password" id="p-c" class="password">
                <button class="unmask" type="button"></button>
            <div id="strong"><span></span></div>
            <div id="valid"></div>
            </p>
            <div class=" row col-md-12">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <a href="myprofile.php" style="background-color: #0C0613; margin-left: 20%" class="btn btn-success btn-block">
                        <i class="fas fa-arrow-left"></i> Profile
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <button type="submit" name="change_pass" style="background-color: #0C0613;" class="btn btn-success btn-block">
                        <i class="fas fa-check"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</body>
<!-- Javascripts & Jquery -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
<script data-cfasync="false" type="text/javascript" src="js/form-submission-handler.js"></script>
</html>
