<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php $_SESSION['TrackingURL'] = $_SERVER["PHP_SELF"]; confirmLogin();?>
<?php
$username = $_SESSION['username'];
$sql = "SELECT * FROM admins WHERE username='$username'";
$result = mysqli_query($connectingDB, $sql);
$row = mysqli_fetch_array($result);
$role = $row["role"];

if($role == "admin"){
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CodeWarriors | Dashboard</title>
    <meta charset="UTF-8">
    <!-- Favicon -->
    <link href="images/first.jpg" rel="shortcut icon"/>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="css/about.css"/>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="css/index.css"/>
    <link rel="stylesheet" href="css/dashboard.css"/>
    <style>
        .list-wrapper {
            max-width: 400px;
            margin: 50px auto;
        }
        .list {
            background: #fff;
            border-radius: 2px;
            list-style: none;
            padding: 10px 20px;
        }
        .list-item {
            display: flex;
            margin: 10px;
            padding-bottom: 5px;
            padding-top: 5px;
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        }
        .list-item:last-child {
            border-bottom: none;
        }
        .list-item-image {
            border-radius: 50%;
            width: 70px;
            height: 70px;
        }
        .list-item-content {
            margin-left: 20px;
        }
        .list-item-content h4, .list-item-content p {
            margin: 0;
        }
        .list-item-content h4 {
            margin-top: 10px;
            font-size: 18px;
        }
        .list-item-content p {
            margin-top: 5px;
            color: #aaa;
        }
    </style>
</head>
<div>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>
<!-- Header section -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="posts.php" class="nav-links2">Posts</a></li>
                <li><a href="categories.php" class="nav-links2">Categories</a></li>
                <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                <li><a href="comments.php" class="nav-links2">Comments</a></li>
                <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                <li><a href="login.php" class="nav-links3">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div style="height:50px; background:#27aae1;"></div>
<!--NAVBAR END-->
<!--HEADER-->

<header class="text-white py-3" style="padding-top: 10px; background-color: #0C0613; height: 145px;">
    <hr class="colorgraph" style="width: 100%!important;">
    <div class="container3">
        <div class="row mt-4" style="width: 100%; margin-top: 0.4rem!important;">
            <div class="col-sm-3 d-none d-md-block">
                <h2 style="padding-top: 10px; font-family: 'Raleway', sans-serif;!important;"><i class="fas fa-cog" style="color: #27aae1;"></i> Dashboard</h2>
            </div>
            <div class="col-sm-9">
                <div class="row mt-4" style="margin-top: 0.8rem!important;">
                    <div class="col-sm-3 mb-2">
                        <a href="categories.php" class="btn btn-info btn-block">
                            <i class="fas fa-edit"></i> Add New Category
                        </a>
                    </div>
                    <div class="col-sm-3 mb-2">
                        <a href="admins.php" class="btn btn-warning btn-block">
                            <i class="fas fa-user-plus"></i> Manage Users
                        </a>
                    </div>
                    <div class="col-sm-3 mb-2">
                        <a href="comments.php" class="btn btn-success btn-block">
                            <i class="fas fa-check"></i> Approve Comments
                        </a>
                    </div>
                    <div class="col-sm-3 mb-2">
                        <a href="#" class="btn btn-block">
                            <div id="MyClockDisplay" style="width: 100%;" class="clock" onload="showTime()" style="background-color: #0C0613"></div>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--HEADER END-->
<br>

<!--MAIN AREA-->
<section class="container3 py-2 mb-4" style="width: 100%">
    <div class="row" style="width: 100%;">
        <div class="col-lg-3 d-none d-md-block">
            <h2 style="font-family: 'Raleway', sans-serif;!important;"><span style="font-weight: bold; padding-bottom: 25px;">Top</span> Users</h2>
            <div style="height: 25px;"></div>
            <div class="card">
            <div class="list-wrapper" ng-app="app" ng-controller="MainCtrl as ctrl" style="margin: 0!important;">
                <ul class="list">
                    <?php
                        $sql = "select username, aimage, count(p.id) as no_posts from admins a
                                                    left join posts p on p.user_id = a.id
                                                        group by a.id
                                                        order by no_posts desc limit 0, 6;";
                        $result = mysqli_query($connectingDB, $sql);
                        while ($row = mysqli_fetch_array($result)){
                    ?>
                    <div class="trending-widget">
                        <div class="tw-item" style="border-left: 5px solid #27aae1; margin-bottom: 10px">
                            <li class="list-item">
                                <div>
                                    <img src="upload/<?php echo $row[1]; ?>" class="list-item-image">
                                </div>
                                <div class="list-item-content">
                                    <h5 style="color: #0C0613; font-family: 'Raleway', sans-serif;!important;">Username:<br>
                                        <span style="font-weight: bold"><?php echo $row[0]; ?></span>
                                    </h5>
                                    <p></p>
                                    <h5 style="color: #0C0613; font-family: 'Raleway', sans-serif;!important;">No. posts:
                                        <span style="font-weight: bold"><?php echo $row[2]; ?></span>
                                    </h5>
                                </div>
                            </li>
                        </div>
                    </div>
                            <hr>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-xs-12" style="width: 100%; padding-left: 20%; padding-right: 20%; padding-bottom: 25px;">
                <a href="statistics.php"
                    <span class="btn btn-primary" style="width: 100%;">Go to Statistics &rang;&rang;</span>
                </a>
            </div>
            </div>
        </div>
        <div class="col-lg-9">
            <?php echo errorMessage(); successMessage(); ?>
            <h2 style="color: #0C0613; font-family: 'Raleway', sans-serif;!important;"><span style="font-weight: bold"">Latest</span> Posts</h2><br>
            <table class="table table-striped table-hover">
                <thead class="table-dark" style="background-color: #0C0613">
                <tr style="font-weight: bold">
                    <td>#</td>
                    <td>Title</td>
                    <td>Category</td>
                    <td>Author</td>
                    <td>Details</td>
                </tr>
                </thead>
                <?php
                global $connectingDB;
                $num_post = 0;
                $sql= "SELECT * FROM posts p ORDER BY p.id desc limit 0, 6;";
                $result = mysqli_query($connectingDB, $sql);
                while ($DataRows = mysqli_fetch_array($result)) {
                    $id       = $DataRows[0];
                    $title    = $DataRows['title'];
                    $dateTime = $DataRows['dateTime'];
                    $category = $DataRows['category_id'];
                    $image    = $DataRows['image'];
                    $user_id  = $DataRows["user_id"];
                    $num_post++;
                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo $num_post; ?></td>
                        <td><?php if (strlen($title) > 70) { $title = substr($title, 0, 70)."..."; } echo $title; ?></td>
                        <td>
                            <?php
                            global $connectingDB;
                            $sql = "SELECT * FROM category WHERE id=$category ";
                            $res = mysqli_query($connectingDB, $sql);
                            $row = mysqli_fetch_array($res);
                            echo htmlentities($row["title"]);
                            ?>
                        </td>
                        <td>
                            <?php
                            global $connectingDB;
                            $sql = "select * from admins where id = $user_id";
                            $res = mysqli_query($connectingDB, $sql);
                            $Row = mysqli_fetch_array($res);
                            echo htmlentities($Row["aname"]);
                            ?>
                        </td>
                        <td>
                            <a href="fullPost.php?id=<?php echo $id; ?>"
                            <span class="btn btn-info">Preview</span>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                <?php }
                ?>
            </table>
            <br>
            <div class="col-sm-9" style="padding-left: 0!important; width: 100%; min-width: 100%;">
                <div class="row mt-4" style="margin-top: 0.8rem!important; width: 100%;">
                    <div class="col-sm-3 mb-2">
                        <div class="column half_whole">
                            <article class="card box_panel" style="color: #0C0613">
                                <section class="card_body" style="padding-top: 15px;">
                                    <div class="chart" data-percent="<?php echo totalAdmins(); ?>">
                                        <div class="knob_data">
                                            <span style="font-size: 3rem!important; font-weight: bolder"><?php echo totalAdmins(); ?></span>
                                        </div>
                                    </div>
                                </section>
                                <br>
                                <section class="stats stats_row">
                                    <div class="stats_item half_whole small_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_success">
                                                <i class="fa fa-users" style="color:#0C0613;"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="stats_item half_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_error">
                                                <h4 style="font-weight: bolder;">Users</h4>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </article>
                        </div>
                    </div>
                    <div class="col-sm-3 mb-2">
                        <div class="column half_whole">
                            <article class="card box_panel" style="color: #0C0613">
                                <section class="card_body" style="padding-top: 15px;">
                                    <div class="chart" data-percent="<?php echo totalPosts(); ?>">
                                        <div class="knob_data">
                                            <span style="font-size: 3rem!important; font-weight: bolder"><?php echo totalPosts(); ?></span>
                                        </div>
                                    </div>
                                </section>
                                <br>
                                <section class="stats stats_row">
                                    <div class="stats_item half_whole small_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_success">
                                                <i class="fa fa-sticky-note"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="stats_item half_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_error">
                                                <h4 style="font-weight: bolder;">Posts</h4>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </article>
                        </div>
                    </div>
                    <div class="col-sm-3 mb-2">
                        <div class="column half_whole">
                            <article class="card box_panel" style="color: #0C0613">
                                <section class="card_body" style="padding-top: 15px;">
                                    <div class="chart" data-percent="<?php echo totalCategories(); ?>"">
                                        <div class="knob_data">
                                            <span style="font-size: 3rem!important; font-weight: bolder"><?php echo totalCategories(); ?></span>
                                        </div>
                                    </div>
                                </section>
                                <br>
                                <section class="stats stats_row">
                                    <div class="stats_item half_whole small_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_success">
                                                <i class="fa fa-list-alt"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="stats_item half_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_error">
                                                <h4 style="font-weight: bolder;">Categories</h4>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </article>
                        </div>
                    </div>
                    <div class="col-sm-3 mb-2">
                        <div class="column half_whole">
                            <article class="card box_panel" style="color: #0C0613">
                                <section class="card_body" style="padding-top: 15px;">
                                    <div class="chart" data-percent="<?php echo totalComments(); ?>">
                                        <div class="knob_data">
                                            <span style="font-size: 3rem!important; font-weight: bolder"><?php echo totalComments(); ?></span>
                                        </div>
                                    </div>
                                </section>
                                <br>
                                <section class="stats stats_row">
                                    <div class="stats_item half_whole small_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_success">
                                                <i class="fa fa-comment"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="stats_item half_whole">
                                        <div class="txt_faded">
                                            <div class="txt_serif stats_item_number txt_error">
                                                <h4 style="font-weight: bolder;">Comments</h4>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </article>
                        </div>
                    </div>
            </div>
<br>

</div>
</section>
<!--END MAIN AREA-->
<!--FOOTER-->
<?php require_once("includes/footer.php"); ?>
<!--FOOTER END-->
</body>
<!-- Javascripts & Jquery -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="http://rendro.github.io/easy-pie-chart/javascripts/jquery.easy-pie-chart.js"></script>
<script src="js/main.js"></script>
</html>
<?php } else {
    $_SESSION["ErrorMessage"] = "You are not allowed to do this operation";
    header("location: blog.php?page=1");
} ?>
