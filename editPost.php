<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php

$SarchQueryParameter = $_GET['id'];
if(isset($_POST["Submit"])){
    $PostTitle = $_POST["PostTitle"];
    $Category  = $_POST["Category"];
    $Image     = $_FILES["Image"]["name"];
    $Target    = "Uploads/".basename($_FILES["Image"]["name"]);
    $PostText  = $_POST["PostDescription"];
    $Admin     = "CodeWarriors";
    date_default_timezone_set("Europe/Tirane");
    $CurrentTime = time();
    $DateTime    = strftime("%B-%d-%Y %H:%M:%S",$CurrentTime);

    if(empty($PostTitle)){
        $_SESSION["ErrorMessage"]= "Title Cant be empty";
        header("Location: editPost.php?id=$SarchQueryParameter");
    }elseif (strlen($PostTitle)<5) {
        $_SESSION["ErrorMessage"]= "Post Title should be greater than 5 characters";
        header("Location: editPost.php?id=$SarchQueryParameter");
    }elseif (strlen($PostText)>1999) {
        $_SESSION["ErrorMessage"]= "Post Description should be less than than 1000 characters";
        header("Location: editPost.php?id=$SarchQueryParameter");
    }else{
        // Query to Update Post in DB When everything is fine
        if (!empty($_FILES["Image"]["name"])) {    // sigurohemi qe nese modifikojme vetem nje nga fushat e formes imazhi mos te fshihet
            $sql = "UPDATE posts
                  SET title='$PostTitle', category='$Category', image='$Image', post='$PostText'
                  WHERE id='$SarchQueryParameter'";
            move_uploaded_file($_FILES["Image"]["tmp_name"],$Target);
        }else {
            $sql = "UPDATE posts
                  SET title='$PostTitle', category='$Category', post='$PostText'
                  WHERE id='$SarchQueryParameter'";
        }
        $result = mysqli_query($connectingDB, $sql);
        if($result){
            $_SESSION["SuccessMessage"]="Post Updated Successfully";
            header("Location: editPost.php?id=$SarchQueryParameter");
        }else {
            $_SESSION["ErrorMessage"]= "Something went wrong. Try Again !";
            header("Location: editPost.php?id=$SarchQueryParameter");
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <title>CodeWarriors | My Profile</title>
    <style media="screen">
        .heading{
            font-family: Bitter,Georgia,"Times New Roman",Times,serif;
            font-weight: bold;
            color: #005E90;
        }
        .heading:hover{
            color: #0090DB;
        }

        .statistika{
            color:black;
            font-size:20px;
        }
    </style>
</head>
<body>
<!-- NAVBAR -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php?page=1"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <?php
            global $role;
            $username = $_SESSION['username'];
            $sql      = "select * from admins where username = '$username'";
            $result   = mysqli_query($connectingDB, $sql);
            $row      = mysqli_fetch_array($result);
            $role     = $row['role'];

            if ($role === 'user'){
                ?>
                <ul class="sm-nav-menu">
                    <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                    <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                    <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                    <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                    <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                    <li><a href="login.php" class="nav-links3">Logout</a></li>
                </ul>
                <ul style="float:right;" class="navbar-nav ml-auto">
                    <form class="form-inline d-none d-sm-block" action="blog.php">
                        <div class="form-group">
                            <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                            <button  class="btn btn-primary" name="SearchButton">Go</button>
                        </div>
                    </form>
                </ul>
            <?php }
            elseif ($role === 'admin') { ?>
                <nav class="sm-navbar navbar navbar-expand-lg">
                    <div class="container2">
                        <div class="sm-logo">
                            <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
                            <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                                <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                                <li><a href="posts.php" class="nav-links2">Posts</a></li>
                                <li><a href="categories.php" class="nav-links2">Categories</a></li>
                                <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                                <li><a href="comments.php" class="nav-links2">Comments</a></li>
                                <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                                <li><a href="login.php" class="nav-links3">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            <?php } ?>
        </div>
    </div>
</nav>
<div style="height:52px; background:#27aae1;"></div>
<!--NAVBAR END-->

<!-- NAVBAR END -->
<!--NAVBAR END-->

<!--HEADER-->
<header class="bg-dark text-white py-3">
    <hr class="colorgraph" style="width: 100%!important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="margin-top:-100px;" ><i class="fas fa-edit" style="color: #27aae1;"></i> Edit Post</h1>
            </div>
        </div>
    </div>
</header>
<!--HEADER END-->
<br>

<!--MAIN AREA-->
<!-- Main Area -->
<section class="container py-2 mb-4">
    <div class="row">
        <div class="offset-lg-1 col-lg-10" style="min-height:400px;">
            <?php
            echo ErrorMessage();
            echo SuccessMessage();

            $SarchQueryParameter = $_GET["id"];
            //$SarchQueryParameter = 18;
            $sql  = "SELECT * FROM posts WHERE id=$SarchQueryParameter";
            $result = mysqli_query($connectingDB, $sql);
            $DataRows = mysqli_fetch_array($result);
            $TitleToBeUpdated    = $DataRows['title'];
            $CategoryId          = $DataRows['category_id'];
            $ImageToBeUpdated    = $DataRows['image'];
            $PostToBeUpdated     = $DataRows['post'];
            ?>
            <form class="" action="editPost.php?id=<?php echo $SarchQueryParameter; ?>" method="post" enctype="multipart/form-data">
                <div class="card bg-secondary text-light mb-3">
                    <div class="card-body bg-dark">
                        <div class="form-group">
                            <label for="title"> <span class="fieldInfo"> Post Title: </span></label>
                            <input class="form-control" type="text" name="PostTitle" id="title" placeholder="Type title here" value="<?php echo $TitleToBeUpdated; ?>">
                        </div>
                        <div class="form-group">
                            <span class="fieldInfo">Existing Category: </span>
                            <span class="fieldInfo" style="color:#00FFFF"> <?php
                                $sql = "SELECT * FROM category WHERE id=$CategoryId";
                                $res = mysqli_query($connectingDB, $sql);
                                $row = mysqli_fetch_array($res);
                                echo $row["title"];
                                ?> </span>
                            <br>
                            <label for="CategoryTitle"> <span class="fieldInfo"> Chose Categroy </span></label>
                            <select class="form-control" id="CategoryTitle"  name="Category">
                                <?php
                                //Fetchinng all the categories from category table
                                $sql  = "SELECT id,title FROM category";
                                $result = mysqli_query($connectingDB, $sql);
                                while ($DataRows = mysqli_fetch_array($result)) {
                                    $Id            = $DataRows["id"];
                                    $CategoryName  = $DataRows["title"];
                                    ?>
                                    <option> <?php echo $CategoryName; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form=group mb-1">
                            <span class="fieldInfo">Existing Image: </span>
                            <img src="upload/<?php echo $ImageToBeUpdated; ?>" class="mb-1" width="170px;" height="100px"><br><br>
                            <div class="custom-file">
                                <input class="custom-file-input" type="File" name="Image" id="imageSelect" value="">
                                <label for="imageSelect" class="custom-file-label">Select Image </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Post"> <span class="fieldInfo"> Post: </span></label>
                            <textarea class="form-control" id="Post" name="PostDescription" rows="8" cols="80">
                <?php echo $PostToBeUpdated;?>
              </textarea>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 mb-2">
                                <a href="#" class="btn btn-warning btn-block"><i class="fas fa-arrow-left"></i> Back To Your Posts</a>
                            </div>
                            <div class="col-lg-6 mb-2">
                                <button type="submit" name="Submit" class="btn btn-success btn-block">
                                    <i class="fas fa-check"></i> &nbsp;  EDIT
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--END MAIN AREA-->

<!--FOOTER-->
<?php require_once("includes/footer.php"); ?>

<!--FOOTER END-->

</body>
</html>
</body>
</html>