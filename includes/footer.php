<!--FOOTER-->
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link href="css/footer.css" rel="stylesheet">
<!-- Footer -->
<footer class="footer-section">
    <div class="container">
        <div class="footer-cta pt-5 pb-5">
            <div class="row" style="text-align: left">
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="cta-text">
                            <h4>Find</h4>
                            <span>1010 Tirane, Albania</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="fas fa-phone"></i>
                        <div class="cta-text">
                            <h4>Call</h4>
                            <span>+355 68 ** ** ***</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="far fa-envelope-open"></i>
                        <div class="cta-text">
                            <h4>Mail</h4>
                            <span>infocodewarriors@gmail.com</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-content pt-5 pb-5">
            <div class="row">
                <div class="col-xl-4 col-lg-4 mb-50">
                    <div class="footer-widget">
                        <div class="footer-logo">
                            <a href="login.php"><img src="images/cw.png" class="img-fluid" alt="logo"></a>
                        </div>
                        <div class="footer-social-icon">
                            <span>Follow us</span>
                            <a href="#"><i class="fab fa-facebook-f facebook-bg social_size"></i></a>
                            <a href="#"><i class="fab fa-instagram twitter-bg social_size"></i></a>
                            <a href="#"><i class="fab fa-google-plus-g google-bg social_size"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Useful Links</h3>
                        </div>
                        <ul>
                            <li><a href="blog.php">Home</a></li>
                            <li><a href="myprofile.php">My Profile</a></li>
                            <li><a href="posts_user.php">Posts</a></li>
                            <li><a href="contactus.php">Contact</a></li>
                            <li><a href="aboutus.php">About us</a></li>
                            <li><a href="login.php">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Subscribe</h3>
                        </div>
                        <div class="footer-text mb-25">
                            <p>Don’t miss to subscribe to our new feeds, kindly fill the form below.</p>
                        </div>
                        <div class="subscribe-form">
                            <form action="#">
                                <input type="text" placeholder="Email Address">
                                <button><i class="fab fa-telegram-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Copyright -->
<div style="height: 2px; background: #27aae1"></div>
<footer class="bg-dark text-white">
    <div class="footer-bottom py-2">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center m-0">© Copyright 2020 <b><a class="color" href="#">CodeWarriors</a></b>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--FOOTER END-->