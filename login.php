<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php require_once("includes/login_handler.php"); ?>
<?php require_once("includes/register_handler.php"); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeWarriors | Log In</title>
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<div id="preloder">
    <div class="loader"></div>
</div>
<!--Container-->
<div class="container">
    <div class="card"></div>
    <div class="card login_card">
        <h1 class="title">Login</h1>
        <?php
            echo errorMessage();
            echo successMessage();
        ?>
        <form action="login.php" method="post">
            <div class="input-container"><input type="text" name="username" id="username" required="required" /><label>Username</label>
                <div class="bar"></div>
            </div>
            <div class="input-container"><input type="password" name="password" id="password" required="required" /><label>Password</label>
                <div class="bar"></div>
            </div>
            <div class="button-container"><button formnovalidate class="btn btn-login btn-block" name="login"><span>LogIn</span></button></div>
            <div class="login-sec">
                <div class="copy-text">Created with <i class="fa fa-heart"></i> by <b><a href="index.php">CodeWarriors</a></b></div>
            </div>
        </form>
    </div>
    <div class="card alt register_card">
        <div class="toggle register_form"></div>
        <h1 class="title">Register
            <div class="close"></div>
        </h1>
        <?php
            echo errorMessage();
            echo successMessage();
        ?>
        <form action="login.php" method="post">
            <div class="input-container"><input type="text" name="name_reg" id="name_reg" required="required" /><label>Name</label>
                <div class="bar"></div>
            </div>
            <div class="input-container"><input type="text" name="username_reg" id="username_reg" required="required" /><label>Username</label>
                <div class="bar"></div>
            </div>
            <div class="input-container"><input type="text" name="email_reg" id="email_reg" required="required" /><label>Email</label>
                <div class="bar"></div>
            </div>
            <div class="input-container"><input type="password" name="password_reg" id="password_reg" required="required" /><label>Password</label>
                <div class="bar"></div>
            </div>
            <div class="input-container"><input type="password" name="confirm_password_reg" id="confirm_password_reg" required="required" /><label>Confirm Password</label>
                <div class="bar"></div>
            </div>
            <div class="button-container"><button formnovalidate class="btn btn-login btn-block" id="login_register_button" name="register"><span>SignUp</span></button></div>
        </form>
    </div>
</div>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
<script>
    $('.toggle').on('click', function() {
        $('.container').stop().addClass('active');
        $('.card .login_card').hide();
        $('.card .alt .register_card').show();
    });

    $('.close').on('click', function() {
        $('.container').stop().removeClass('active');
        $('.card .alt .register_card').hide();
        $('.card login_card').show();
    });
</script>
</html>