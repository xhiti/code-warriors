<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php
    $_SESSION["TrackingURL"]=$_SERVER["PHP_SELF"];   // kthen URL e faqes aktuale
    confirmLogin();
?>
<?php
$username = $_SESSION['username'];
$sql = "SELECT * FROM admins WHERE username='$username'";
$result = mysqli_query($connectingDB, $sql);
$row = mysqli_fetch_array($result);
$role = $row["role"];

if($role == "admin"){
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CodeWarriors | Posts</title>
    <meta charset="UTF-8">
    <!-- Favicon -->
    <link href="images/first.jpg" rel="shortcut icon"/>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="css/about.css"/>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="css/index.css"/>
</head>
    <!-- Header section -->
    <nav class="sm-navbar navbar navbar-expand-lg">
        <div class="container2">
            <div class="sm-logo">
                <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbarcollapseCMS">
                <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                    <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                    <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                    <li><a href="posts.php" class="nav-links2">Posts</a></li>
                    <li><a href="categories.php" class="nav-links2">Categories</a></li>
                    <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                    <li><a href="comments.php" class="nav-links2">Comments</a></li>
                    <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                    <li><a href="login.php" class="nav-links3">Logout</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div style="height:50px; background:#27aae1;"></div>
<header class="bg-dark text-white py-3">
    <hr class="colorgraph" style="width: 100%!important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="margin-top:-100px;" ><i class="fas fa-sticky-note" style="color: #27aae1;"></i> Manage Posts</h1>
            </div>
        </div>
    </div>
</header>
<!--HEADER-->
<div class="container2">
    <div class="row mt-4" style="width: 100%;">
        <!-- Main Area Start-->
        <div class="col-sm-12"> <br><br><br>
            <h2 style="color: #0C0613; padding-left: 10px; margin-top: -60px;">All <span style="font-weight: bold">Posts</span></h2><br><br>
            <div class="col-12">
                <?php echo errorMessage(); echo successMessage(); ?>
                <table class="table table-striped table-hover">
                    <thead class="table-dark" style="background-color: #0C0613">
                    <tr style="font-weight: bold">
                        <td>#</td>
                        <td>Title</td>
                        <td>Category</td>
                        <td>Date&Time</td>
                        <td>Author</td>
                        <td>Banner</td>
                        <td>Actions</td>
                        <td>Live Preview</td>
                    </tr>
                    </thead>
                    <?php
                    global $connectingDB;
                    $num_post = 0;
                    $sql= "SELECT * FROM posts p ORDER BY p.id desc;";
                    $result = mysqli_query($connectingDB, $sql);
                    while ($DataRows = mysqli_fetch_array($result)) {
                        $id       = $DataRows[0];
                        $title    = $DataRows['title'];
                        $dateTime = $DataRows['dateTime'];
                        $category = $DataRows['category_id'];
                        $image    = $DataRows['image'];
                        $user_id  = $DataRows["user_id"];
                        $num_post++;
                        ?>
                        <tbody>
                        <tr>
                            <td><?php echo $num_post; ?></td>
                            <td><?php if (strlen($title) > 30) { $title = substr($title, 0, 30)."..."; } echo $title; ?></td>
                            <td>
                                <?php
                                global $connectingDB;
                                $sql = "SELECT * FROM category WHERE id=$category ";
                                $res = mysqli_query($connectingDB, $sql);
                                $row = mysqli_fetch_array($res);
                                echo htmlentities($row["title"]);
                                ?>
                            </td>
                            <td><?php if (strlen($dateTime) > 11) { $dateTime = substr($dateTime, 0, 11)."..."; } echo $dateTime; ?></td>
                            <td>
                                <?php
                                global $connectingDB;
                                $sql = "select * from admins where id = $user_id";
                                $res = mysqli_query($connectingDB, $sql);
                                $Row = mysqli_fetch_array($res);
                                echo htmlentities($Row["aname"]);
                                ?>
                            </td>
                            <td><img src="upload/<?php echo $image; ?>" width="100px;" height="60px"></td>
                            <td>
                                <a href="deletePost.php?id=<?php echo $id;?>" class="btn btn-danger">Delete</a>                            </td>
                            </td>
                            <td><a href="fullPost.php?id=<?php echo $id; ?>" target="_blank"><span class="btn btn-primary">Live Preview</span></a></td>
                        </tr>
                        </tbody>
                    <?php }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
<br><br><br>
<!--END MAIN AREA-->

<?php require("includes/footer.php"); ?>
</body>
</html>
<?php } else {
    $_SESSION["ErrorMessage"] = "You are not allowed to do this operation";
    header("location: blog.php?page=1");
} ?>
