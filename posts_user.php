<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php confirmLogin(); ?>

<?php
    //$user_id = $_POST['id'];
    $username = $_SESSION['username'];
    $sql = "select * from admins where username = '$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $user_id = $row[0];

    if ($result){
        $name      = $row['aname'];
        $age       = $row['age'];
        $email     = $row['email'];
        $location  = $row['location'];
        $bio       = $row['abio'];
        $interests = $row['aheadline'];
    }
?>
<?php
$username = $_SESSION['username'];
$sql = "SELECT * FROM admins WHERE username='$username'";
$result = mysqli_query($connectingDB, $sql);
$row = mysqli_fetch_array($result);
$role = $row["role"];

if($role == "user"){
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <title>CodeWarriors | My Posts</title>
</head>
<body>
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php?page=1"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu">
                <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                <li><a href="login.php" class="nav-links3">Logout</a></li>
            </ul>
            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
</nav>
<div style="height:70px; background:#27aae1;"></div>
<!-- NAVBAR END -->
<!-- HEADER -->
<div class="container2">
    <div class="row mt-4">
        <!-- Main Area Start-->
        <div class="col-sm-9"> <br><br><br>
            <h2 style="color: #0C0613; padding-left: 10px; margin-top: -60px;">My <span style="font-weight: bold">Posts</span></h2><br><br>
            <div class="col-12">
                <?php echo errorMessage(); echo successMessage(); ?>
                <table class="table table-striped table-hover">
                    <thead class="table-dark" style="background-color: #0C0613">
                    <tr style="font-weight: bold">
                        <td>#</td>
                        <td>Title</td>
                        <td>Category</td>
                        <td>Date&Time</td>
                        <td>Banner</td>
                        <td>Actions</td>
                        <td>Live Preview</td>
                    </tr>
                    </thead>
                    <?php
                    global $connectingDB;
                    $num_post = 0;
                    $sql= "SELECT * FROM posts p join admins a on a.id = p.user_id where a.id = $user_id ORDER BY p.id desc;";
                    $result = mysqli_query($connectingDB, $sql);
                    while ($DataRows = mysqli_fetch_array($result)) {
                        $id       = $DataRows[0];
                        $title    = $DataRows['title'];
                        $dateTime = $DataRows['dateTime'];
                        $category = $DataRows['category_id'];
                        $image    = $DataRows['image'];
                        $num_post++;
                        ?>
                        <tbody>
                        <tr>
                            <td><?php echo $num_post; ?></td>
                            <td><?php if (strlen($title) > 30) { $title = substr($title, 0, 30)."..."; } echo $title; ?></td>
                            <td>
                                <?php
                                    global $connectingDB;
                                    $sql = "SELECT * FROM category WHERE id=$category ";
                                    $res = mysqli_query($connectingDB, $sql);
                                    $row = mysqli_fetch_array($res);
                                    echo htmlentities($row["title"]);
                                    ?>
                            </td>
                            <td><?php if (strlen($dateTime) > 11) { $dateTime = substr($dateTime, 0, 11)."..."; } echo $dateTime; ?></td>
                            <td><img src="upload/<?php echo $image; ?>" width="100px;" height="60px"></td>
                            <td>
                                <a href="editPost.php?id=<?php echo $id; ?>"><span class="btn btn-warning">Edit</span></a>
                                <a href="deletePost.php?id=<?php echo $id;?>" class="btn btn-danger">Delete</a>                            </td>
                            </td>
                            <td><a href="fullPost.php?id=<?php echo $id; ?>" target="_blank"><span class="btn btn-primary">Live Preview</span></a></td>
                        </tr>
                        </tbody>
                    <?php }
                    ?>
                </table>
            </div>
        </div>
        <!-- Main Area End-->

        <!-- Side Area Start -->
        <div class="col-sm-3" style="padding-top: 10px;">
            <h2 style="color: #0C0613; float: right;"><span style="font-weight: bold">Code</span>Warriors</h2><br><br><br>
            <div class="card mt-4">
                <img src="./images/startblog.PNG" alt="">
                <div class="card-body">
                    <p>Start a blog now & create your posts!</p>
                    <p>Make a post in our blog to share it with your friends and find out even more or you just want to publish your achievement?</p>
                    <p>Share your ideas and much more!</p>
                    <a href="addNewPost.php" style="padding-left: 75px;">
                        <span class="btn btn-primary">Create/Add Post</span>
                    </a>
                </div>
            </div>
            <br>
            <a href="blog.php?page=1" class="site-btn" id="contact_us" name="go_to_blog" style="text-align: center; margin-left: 15%;">Go to Blog Page<img src="images/icons/double-arrow.png" alt="#"/></a><br><br><br>
            <a href="myprofile.php" class="site-btn" id="contact_us" name="go_to_blog" style="text-align: center; margin-left: 15%;">Go to MyProfile  <img src="images/icons/double-arrow.png" alt="#"/></a>
        </div>
        <!-- Side Area End -->
    </div>
</div>
<br><br><br>
<?php require("includes/footer.php"); ?>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</html>
<?php }
else{ header("location: dashboard.php");?>
    <div class="container">
        <?php $_SESSION["ErrorMessage"] = "You are not allowed to do this operation"; ?>
    </div>
<?php  }?>