<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php echo confirmLogin(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeWarriors | Blog Page</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link href="./css/statistika.css" rel="stylesheet"></head>

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/my.css">
    <style>
      .not{
        color:grey;
      }
      .fa:hover {
       color: darkblue;
      }

      .statistika{
        color:black;
        font-size:20px;
      }

      #logouti{
        color: darkblue;
      }

      .heading{
          font-family: Bitter,Georgia,"Times New Roman",Times,serif;
          font-weight: bold;
           color: #005E90;
      }
      .heading:hover{
        color: #0090DB;
      }

      .infos:hover{
        color: #0090DB;
      }


    </style>

</head>
<body>

<!-- HEADER START -->
  <nav class="sm-navbar navbar navbar-expand-lg">
        <div class="container2">
            <div class="sm-logo">
                <a href="blog.php?page=1" style="margin-left:-40px"><img src="images/cw.png" width="110px" height="40px"></a>
            </div>

            <div class="collapse navbar-collapse" id="navbarcollapseCMS">
              <?php
              if (confirmLogin() == true){

                $ID = $_SESSION["userid"];
                $sql      = "select * from admins where id = $ID";
                $result   = mysqli_query($connectingDB, $sql);
                $row      = mysqli_fetch_array($result);
                $role     = $row['role'];

                if ($role === 'user'){
              ?>
              <ul class="sm-nav-menu" style="margin-left:20px;">
                  <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                  <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i>&nbsp; My Profile</a></li>
                  <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                  <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                  <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                  <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
              </ul>

            <?php } elseif ($role == 'admin'){ ?>
              <ul class="sm-nav-menu" style="margin-left:20px;">
                  <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                  <li><a href="posts.php" class="nav-links2">Posts</a></li>
                  <li><a href="categories.php" class="nav-links">Categories</a></li>
                  <li><a href="manageUsers.php" class="nav-links2">Users</a></li>
                  <li><a href="comments.php" class="nav-links2">Comments</a></li>
                  <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i> &nbsp;My Profile</a></li>
                  <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
              </ul>

            <?php } ?>
          <?php } ?>


            </div>
  </nav>
  <!-- HEADER END -->

<div style="height: 70px; background: #27aae1"></div>
<!-- NAVBAR END -->

<!--HEADER-->
<header class="bg-dark text-white py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="margin-top:-100px;"><i class="fa fa-bar-chart" style="font-size:40px; color:#27aae1;"></i> Statistics about our Blog</h1>
            </div>
        </div>
    </div>
</header>
<!--HEADER END-->

<br>







<div class="container-fluid">

  <div class="row">
                            <div class="col-md-6 col-xl-4">
                                <div class="card mb-3 widget-content bg-midnight-bloom">
                                    <div class="widget-content-wrapper text-white">
                                        <div class="widget-content-left">
                                            <div style="margin-right:30px; font-size:25px;" class="widget-heading">Total Posts</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-white"><span> <?php
                                                                                            $connectingDB;
                                                                                            $sql = "SELECT COUNT(*) FROM posts";
                                                                                            $result = mysqli_query($connectingDB, $sql);
                                                                                            $row = mysqli_fetch_array($result);
                                                                                            echo $row[0];
                                                                                           ?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card mb-3 widget-content bg-arielle-smile">
                                    <div class="widget-content-wrapper text-white">
                                        <div class="widget-content-left">
                                            <div style="margin-right:30px; font-size:25px;" class="widget-heading">Total Users</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-white"><span> <?php
                                                                                           $connectingDB;
                                                                                           $sql = "SELECT COUNT(*) FROM admins";
                                                                                           $result = mysqli_query($connectingDB, $sql);
                                                                                           $row = mysqli_fetch_array($result);
                                                                                           echo $row[0];
                                                                                           ?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="card mb-3 widget-content bg-grow-early">
                                    <div class="widget-content-wrapper text-white">
                                        <div class="widget-content-left">
                                            <div style="margin-right:30px; font-size:25px;" class="widget-heading">Total Categories</div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-white"><span> <?php
                                                                                          $sql = "SELECT COUNT(*) FROM category";
                                                                                          $result = mysqli_query($connectingDB, $sql);
                                                                                          $row = mysqli_fetch_array($result);
                                                                                          echo $row[0];
                                                                                          ?></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>



  <div class="row">
    <div class="col ">

      <!-- MOST LIKED POSTS -->
      <div class="card">
        <div class="card-header bg-info text-white">
          <h2 class="lead"> TOP 5 MOST LIKED POSTS</h2>
        </div>
        <div class="card-body">
          <?php
          global $connectingDB;
          $sql= "SELECT * FROM rating_info ri JOIN posts p ON ri.post_id=p.id
                 WHERE rating_action='like'
                GROUP BY post_id
                HAVING COUNT(*) > 0";
          $result = mysqli_query($connectingDB, $sql);
          while ($DataRows = mysqli_fetch_array($result)) {
            $Id     = $DataRows['id'];
            $Title  = $DataRows['title'];
            $DateTime = $DataRows['dateTime'];
            $Image = $DataRows['image'];
          ?>
          <div class="media">
            <img src="upload/<?php echo htmlentities($Image); ?>" class="d-block img-fluid align-self-start"  width="90" height="94" alt="">
            <div class="media-body ml-2">
            <a style="text-decoration:none;" href="FullPost.php?id=<?php echo htmlentities($Id) ; ?>" target="_blank">  <h6 class="lead"><?php echo htmlentities($Title); ?></h6> </a>
              <p class="small"><?php echo htmlentities($DateTime); ?></p>
            </div>
          </div>
          <hr>
          <?php } ?>
        </div>
      </div>

    </div>
    <div class="col">

      <div class="card">
        <div class="card-header bg-info text-white">
          <h2 class="lead"> TOP 5 MOST COMMENTED POSTS</h2>
        </div>
        <div class="card-body">
          <?php
          global $connectingDB;
          $sql= "SELECT * FROM comments c JOIN posts p ON c.post_id=p.id
                GROUP BY post_id
                HAVING COUNT(*) > 0";
          $result = mysqli_query($connectingDB, $sql);
          while ($DataRows = mysqli_fetch_array($result)) {
            $Id     = $DataRows['id'];
            $Title  = $DataRows['title'];
            $DateTime = $DataRows['dateTime'];
            $Image = $DataRows['image'];
          ?>
          <div class="media">
            <img src="upload/<?php echo htmlentities($Image); ?>" class="d-block img-fluid align-self-start"  width="90" height="94" alt="">
            <div class="media-body ml-2">
            <a style="text-decoration:none;" href="FullPost.php?id=<?php echo htmlentities($Id) ; ?>" target="_blank">  <h6 class="lead"><?php echo htmlentities($Title); ?></h6> </a>
              <p class="small"><?php echo htmlentities($DateTime); ?></p>
            </div>
          </div>
          <hr>
          <?php } ?>
        </div>
      </div>

    </div>

    <div class="col">

      <div class="card">
        <div class="card-header bg-info text-white">
          <h2 class="lead"> MOST LIKED CATEGORIES</h2>
        </div>
          <div class="card-body">
            <?php
            global $connectingDB;
            $sql= "SELECT * FROM  category c, rating_info ri, posts p
                  WHERE ri.post_id = p.id AND p.category_id = c.id AND rating_action='like'
                  GROUP BY category_id
                  HAVING COUNT(*) > 0";

            $result = mysqli_query($connectingDB, $sql);
            while ($DataRows = mysqli_fetch_array($result)){
              $CategoryId = $DataRows[0];
              $CategoryName=$DataRows[1];
             ?>
            <a href="blog.php?category=<?php echo $CategoryId; ?>"> <span class="heading" style="font-size:25px;"> <?php echo $CategoryName; ?></span> </a><br>

           <?php } ?>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
      <div class="card-header bg-info text-white">
        <h2 class="lead" style="width:70%">ALL BLOG USERS</h2>
      </div>
                          <table class="table table-striped table-hover">

                                  <thead class="table-dark">

                                      <tr>
                                          <td>#</td>
                                          <td>Username</td>
                                          <td>Image</td>
                                          <td>Country</td>
                                          <td>Age</td>
                                          <td>Gender</td>
                                          <td>#Posts</td>
                                          <td>#Comments</td>
                                      </tr>
                                  </thead>
                                  <?php
                                      $sql = "select * from admins";
                                      $result = mysqli_query($connectingDB, $sql);

                                      $num_user = 1;
                                      while ($row = mysqli_fetch_array($result)) {
                                          $user_id = $row["id"];
                                          $username = $row["username"];
                                          $image = $row["aimage"];
                                          $gender = $row["gender"];
                                          $age = $row["age"];
                                          $country = $row["location"];
                                          ?>
                                  <tbody>
                                      <tr>
                                          <td><?php echo $num_user; ?></td>
                                          <td><?php echo $username ?></td>
                                          <td><img src="upload/<?php echo htmlentities($image); ?>" class="d-block img-fluid align-self-start"  width="90" height="14" alt=""></td>
                                          <td><?php echo $country; ?></td>
                                          <td><?php echo $age ?></td>
                                          <td><?php echo $gender ?></td>


                                          <td><?php
                                          global $connectingDB;
                                          $query = "SELECT * FROM posts WHERE user_id=$user_id";
                                          $re = mysqli_query($connectingDB, $query);
                                          $n = mysqli_num_rows($re);
                                          echo $n;
                                               ?>
                                               </td>
                                          <td><?php
                                               $qe = "SELECT * FROM comments c
                                                       WHERE user_id = $user_id ";
                                               $res = mysqli_query($connectingDB, $qe);
                                               $nm = mysqli_num_rows($res);
                                               echo $nm;
                                              ?>
                                          </td>
                                      </tr>
                                  </tbody>
                                  <?php $num_user++; } ?>
                              </table>
                            </div>
                        </div>



<!-- FOOTER -->
<?php require_once("includes/footer.php"); ?>
</body>
</html>
